(function () {

    // If we receive a signed request, make sure the signing token is forwarded to outgoing response

    $(document).ready(function () {
        var signedRequest = $("meta[name=acpt]").attr("content");
        $.ajaxSetup({
            beforeSend: function (request) {
                request.setRequestHeader("X-acpt", signedRequest);
            }
        });
    });

})();
