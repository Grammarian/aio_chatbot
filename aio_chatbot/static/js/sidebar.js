(function () {

    var $spinner = document.getElementById("spinner-container");
    $spinner.spin("medium");

    // Listen for incoming updates from the server (via SSE) and display
    // the received HTML in the sidebar container element

    $(document).ready(function () {
        console.log('ready');


        if (typeof(EventSource) == "undefined") {
            console.log('ERROR: Server-sent events not supported')
        } else {
            var baseUrl = $("meta[name=base-url]").attr("content");
            var evtSrc = new EventSource(baseUrl + "sidebar-subscribe");

            evtSrc.onmessage = function (e) {
                var obj = JSON.parse(e.data);
                console.log(obj);

                if ($spinner !== null) {
                    $spinner.data().spinner.stop();
                    $spinner = null;
                }

                var timezonesHtmlContainer = document.getElementById("sidebar-container");
                timezonesHtmlContainer.innerHTML = obj.html;
            };

            evtSrc.onerror = function (e) {
                console.log('ERROR:' + e.data);
            };
        }
    });
})();
