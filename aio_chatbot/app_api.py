import asyncio
import json

from aio_chatbot.chatter_engine import ChatterEngine
from aio_hipchat.hc_addon_api import HcAddonApi
from aio_hipchat.hc_structures import ImageCard, CardNotification, json_value, Notification
from aio_hipchat.log import make_default_logger
from aio_hipchat.pusher import MultiPusher
from aio_hipchat.toolkit import nested_get

from aio_chatbot.hc_client import HcClient
from datetime import datetime

ADDON_KEY = 'simply-brilliant'
GLANCE_KEY = '%s.glance' % ADDON_KEY
SIDEBAR_KEY = '%s.sidebar' % ADDON_KEY

CHATBOT_ID='chatter1'
CHATBOT_MENTION='Chatter'
CHATBOT_MESSAGE_PATH = '/chatbot/incoming'

_logger = make_default_logger(__name__)


# chatbot_url = '%s/bot_message' % LOCAL_BASE_URL
# yield tenant_api_client.create_chatbot(group_id, chatbot_url, "ClinkyBot", "ClinkyBot")

class SampleApi(HcAddonApi):

    api = HcAddonApi

    def __init__(self, app):
        super().__init__(app)

        self.chatbot_id = CHATBOT_ID  # this should probably come from app.config

        app.installed.subscribe(self.post_install)
        app.uninstalled.subscribe(self.post_uninstall)

        # Create a cross-server pusher that will send html to all open sidebars
        # The path given must match the path used in sidebar.js for the EventSource
        # self.multi_server_pusher = MultiPusher.create(self.app, "/sidebar-subscribe", ping_interval=10)

        wrapped_method = self._wrap_method(self.handle_chatbot_callback, require_auth=False, allow_cors=False, extract=False)
        self.app.add_route('POST', CHATBOT_MESSAGE_PATH, wrapped_method, 'chatbot-incoming', expect_handler=None)

    @asyncio.coroutine
    def post_install(self, params):
        """
        This addon has just been installed. Let them know we are here
        """
        installation = params['installation']
        _logger.debug("Post processing install: %s", installation)

        hc_client = HcClient(installation)

        # Tell the room that we are available
        # ic = ImageCard(self.app.relative_to_base('/static/robot-1-2x.png'),
        #                side_by_side=True,
        #                title="Welcome to Chatter",
        #                description="Chatter is a one-on-one chat bot. Type '/chatter' to get started.")
        # yield from hc_client.send_notification(installation.room_id, CardNotification(ic))

        # Register myself with hipchat so we can act as a chatbot
        yield from hc_client.register_chatbot(CHATBOT_ID, self.app.relative_to_base(CHATBOT_MESSAGE_PATH), mention=CHATBOT_MENTION)

    @asyncio.coroutine
    def post_uninstall(self, params):
        """
        This addon has just been uninstalled. Clean up.

        At this point, the installation has already been deleted, so we can't actually do
        anything in HipChat itself.
        """
        installation = params['installation']
        _logger.debug("Post processing uninstall: %s", installation)

        hc_client = HcClient(installation)
        yield from hc_client.unregister_chatbot(self.app.relative_to_base(CHATBOT_MESSAGE_PATH))

    @api.message_hook('^/chatter(\s|$)', "chatter-command", extract=True)
    def handle_chatter_message_hook(self, request):

        hc_client = HcClient(request.installation)
        from_user = nested_get(request.body_as_json, "item", "message", "from")

        # yield from hc_client.send_chatbot_message(self.chatbot_id, from_user.get('id', '??'), "Ok. I'm here. Let's talk")

    @api.message_hook('^/cb(\s|$)', "chatter-msg", extract=True)  # this is only until we have a hook
    def handle_chatbot_message(self, request):

        body_as_json = yield from request.json()
        _logger.debug("body_as_json=%r", body_as_json)

        oauth_client_id = body_as_json.get('oauth_client_id')
        if oauth_client_id:
            oauth = yield from self.app.load_installation(oauth_client_id)
        else:
            oauth = None

        hc_client = HcClient(oauth) if oauth else None

        message = nested_get(body_as_json, "item", "message")
        from_user = nested_get(body_as_json, "item", "message", "from")

        _logger.debug("message=%r", message)
        _logger.debug("from_user=%r", from_user)

        message_text = message.get('message', '')[4:].strip()
        from_user_id = from_user.get('id')

        history_entries = yield from self.get_history(from_user_id)

        thinking_futures = [
            self.app.loop.call_later(2, self._send_thinking, hc_client, from_user_id, "Thinking..."),
            self.app.loop.call_later(5, self._send_thinking, hc_client, from_user_id, "...still thinking..."),
            self.app.loop.call_later(10, self._send_thinking, hc_client, from_user_id, "...this is either going to be really good -- or something is broken..."),
        ]
        engine = ChatterEngine(hc_client, from_user_id, history_entries)
        result = yield from engine.process(message_text)
        for x in thinking_futures:
            x.cancel()

        yield from self.add_to_history(from_user_id, message_text, result)

       # yield from hc_client.send_chatbot_message(self.chatbot_id, from_user_id, result)


    @asyncio.coroutine
    def handle_chatbot_callback(self, request):

        import pprint

        _logger.debug(pprint.pformat(request.headers))

        body_as_json = yield from request.json()
        _logger.debug("body_as_json=%r", body_as_json)

        # body_as_json={
        #     'oauth_client_id': 'dc32a9c2-4d98-4178-866c-01a4a8d8aa3d',
        #     'item': {
        #         '_id': '5c23c4a1-b944-4a3e-a0ca-e19e1d023229',
        #         'from': {
        #             'user_id': 1,
        #             'group_id': 1
        #         },
        #         'stanza_data': {
        #             'body': 'this'
        #         },
        #         'stanza_type': 'message',
        #         'date': '2015-12-11T00:22:45Z 863589',
        #         'to': {
        #             'user_id': 2,
        #             'group_id': 1
        #         },
        #         'privatechat_id': '1_1-1_2'}}

        oauth_client_id = body_as_json.get('oauth_client_id')
        if oauth_client_id:
            oauth = yield from self.app.load_installation(oauth_client_id)
        else:
            oauth = None

        hc_client = HcClient(oauth) if oauth else None

        message_text = nested_get(body_as_json, 'item', 'stanza_data', 'body')
        from_user_id = nested_get(body_as_json, 'item', 'from', 'user_id')
        to_user_id = nested_get(body_as_json, 'item', 'to', 'user_id')
        group_id = nested_get(body_as_json, 'item', 'to', 'group_id')

        history_entries = yield from self.get_history(from_user_id)

        # NOTE: to and from user are reversed! since we are setting back to the originer

        thinking_futures = [
            self.app.loop.call_later(2, self._send_thinking, hc_client, group_id, to_user_id, from_user_id, "Thinking..."),
            self.app.loop.call_later(5, self._send_thinking, hc_client, group_id, to_user_id, from_user_id, "...still thinking..."),
            self.app.loop.call_later(10, self._send_thinking, hc_client, group_id, to_user_id, from_user_id, "...this is either going to be really good -- or something is broken..."),
        ]
        engine = ChatterEngine(hc_client, from_user_id, history_entries)
        result = yield from engine.process(message_text)
        for x in thinking_futures:
            x.cancel()

        yield from self.add_to_history(from_user_id, message_text, result)

        yield from hc_client.send_chatbot_message(group_id, to_user_id, from_user_id, result)

    def _send_thinking(self, hc_client, group_id, from_user_id, to_user_id, msg):

        @asyncio.coroutine
        def inner():
            yield from hc_client.send_chatbot_message(group_id, from_user_id, to_user_id, msg)

        self.app.loop.create_task(inner())

    def make_redis_history_key(self, from_user_id):
        return 'chatter:history:{}'.format(from_user_id)

    @asyncio.coroutine
    def get_history(self, user_id):
        history_key = self.make_redis_history_key(user_id)
        history = yield from self.app.redis.lrange(history_key)
        history_entries = [] # [json.loads(x) for x in history]
        return history_entries

    @asyncio.coroutine
    def add_to_history(self, user_id, message_text, result):
        history_key = self.make_redis_history_key(user_id)
        history_entry = {'message': message_text, 'result': json_value(result)}
        yield from self.app.redis.lpush(history_key, [json.dumps(history_entry)])
