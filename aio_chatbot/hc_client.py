import asyncio
import json
import logging

from aio_hipchat import toolkit as tk
from aio_hipchat.hc_structures import Notification
from aio_hipchat.log import logged

_logger = logging.getLogger(__name__)

DEFAULT_TIMEOUT = 10

class HcClient:

    def __init__(self, oauth, timeout=DEFAULT_TIMEOUT):
        self.oauth = oauth
        self.timeout = timeout

    def _room_base_url(self, room_id):
        return '{api_url}/room/{room_id}'.format(api_url=self.oauth.api_base_url, room_id=room_id)

    def _chatbot_base_url(self, chatbot_id):
        return '{api_url}/chatbot/{chatbot_id}'.format(api_url=self.oauth.api_base_url, chatbot_id=chatbot_id)

    @asyncio.coroutine
    def send_notification(self, room_id, notification):
        url = self._room_base_url(room_id) + "/notification"
        yield from self._send_auth_request(url, notification, "Sending room notification")

    @asyncio.coroutine
    def register_chatbot(self, name, path, mention):
        url = self.oauth.api_base_url + "/message/chatbot"
        data = {
            "name": name,
            "url": path,
            "mention": mention,
            "oauth_client_id": self.oauth.id
        }
        yield from self._send_auth_request(url, data, "Registering chatbot")

    @asyncio.coroutine
    def unregister_chatbot(self, path):
        url = self.oauth.api_base_url + "/message/chatbot"
        data = {
            "url": url,
            "oauth_client_id": self.oauth.id
        }
        yield from self._send_auth_request(url, data, "Unregistering chatbot", 'DELETE')

    @asyncio.coroutine
    @logged(dump_args=True)
    def send_chatbot_message2(self, chatbot_id, user_id, data):
        # notification = Notification(message="If this was working, I would now send to {}".format(user_id))
        # yield from self.send_notification(self.oauth.room_id, notification)

        yield from self.send_notification(self.oauth.room_id, data)
        # url = self._chatbot_base_url(chatbot_id) + "/message/" + str(user_id)
        # yield from self._send_auth_request(url, data, "Sending chatbot message")

    @asyncio.coroutine
    def send_chatbot_message(self, group_id, from_user_id, to_user_id, message):

        url = self.oauth.api_base_url + "/message/bot_message"
        data = {"message": message, "from_user_id": from_user_id, "to_user_id": to_user_id}

        headers = yield from self._auth_headers()
        headers['X-HIPCHAT-GROUP'] = str(group_id)

        yield from self._send_auth_request(url, data, 'Sending chatbot message', headers=headers)

    @asyncio.coroutine
    def _auth_headers(self):
        """
        Create the headers required for an authorized request
        """
        token = yield from self.oauth.get_token()
        headers = {
            'content-type': 'application/json',
            'authorization': 'Bearer %s' % token
        }
        return headers

    @asyncio.coroutine
    def _send_auth_request(self, url, data, operation, method='POST', headers=None):
        data_as_json = data.as_json() if hasattr(data, "as_json") else data
        flat_data = json.dumps(data_as_json)
        _logger.info("%s to %s: %s", operation, url, flat_data)

        if not headers:
            headers = yield from self._auth_headers()
        with (yield from tk.retry_request(method, url, headers=headers, data=flat_data, timeout=self.timeout)) as resp:
            if resp.status not in (200, 204):
                body = yield from resp.read()
                _logger.error("%s failed: %s - %s", operation, resp.status, body)
