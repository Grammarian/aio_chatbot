import asyncio
import logging
import re

from aio_chatbot.wolfram import wolfram_query
from aio_hipchat.hc_structures import Notification, CardNotification, ImageCard, ApplicationCard
from aio_hipchat import toolkit as tk

_logger = logging.getLogger(__name__)

# todo -- move to toolkit
def first_or_none(iterator):
    if iterator is None:
        return None
    try:
        iterator = iter(iterator)
    except TypeError:
        pass
    try:
        return next(iterator)
    except StopIteration:
        return None

def as_ul(iter):
    items = ("- {}".format(x) for x in iter)
    return "{}".format('\n'.join(items))

class _Verb:

    def __init__(self, ptns, description, examples, handler):
        self.patterns = [ptns] if isinstance(ptns, str) else ptns
        self.description = description
        self.examples = examples
        self.handler = handler
        self.regexes = [re.compile(x) for x in self.patterns]


class ChatterEngine:

    def __init__(self, hc_client, user, history):
        self.hc_client = hc_client
        self.user = user
        self.history = history
        self.verbs = {
            "help": _Verb('^(show |get |)help(.*)', 'Show what this bot can do', ['help!', 'get help'], self._help)
        }

    @asyncio.coroutine
    def process(self, message):
        if not message:
            message = 'how much vitamin E is there in a million croissants?'

        verb, other_bits = self.parse_input(message)

        handler = verb.handler if verb else self.default_handler

        result = yield from tk.maybe_coroutine(handler, other_bits)

        return result

    def parse_input(self, message):
        # this should be more intelligent

        _logger.debug('matching against: "%s"', message)
        for verb in self.verbs.values():
            for pattern in verb.regexes:
                match = pattern.search(message)
                if match:
                    other_bit = match.group(1) if len(match.groups()) >= 1 else ''
                    return verb, other_bit.strip()

        return None, message

    @asyncio.coroutine
    def _help(self, other):

        yield from asyncio.sleep(1)
        if other in self.verbs:
            html = self._help_single_command(other)
        else:
            html = self._help_all_commands()

        #return Notification(message=html, message_format='html')
        return html

    def _help_single_command(self, cmd):
        v = self.verbs.get(cmd)
        return v.description + '<br/>' + ['<br/>'.join(v.examples)]

    def _help_all_commands(self):
        html = "<b>Known commands<b><br/>"
        html += as_ul('{} - {}'.format(k, self.verbs[k].description) for k in sorted(self.verbs))
        html += "<br/>If I can't find anything else, I'll get some help from the outside to try and answer your question"
        return html

    @asyncio.coroutine
    def default_handler(self, message):

        result = yield from wolfram_query(message)

        if not result or not result.success:
            return 'I have no idea what you are talking about'

        return result.result_text

    @asyncio.coroutine
    def default_handler_with_notifications(self, message):

        result = yield from wolfram_query(message)

        if not result or not result.success:
            return Notification(message='I have no idea what you are talking about', message_format='text')

        link_url = first_or_none(result.result_sources) or 'http://www.wolframalpha.com/'

        if result.result_img:
            card = ImageCard(
                result.result_img.get('@src'),
                side_by_side=True,
                link_url=link_url,
                description=result.result_text,
                title='Q: %s' % message)
        else:
            card = ApplicationCard(
                title='Q: %s' % message,
                link_url=link_url,
                icon_url='http://products.wolframalpha.com/images/api/spikey.png',
                description=result.result_text)

        return CardNotification(card)


