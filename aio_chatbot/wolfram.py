import asyncio
import urllib
import xmltodict
import logging

from aio_hipchat import toolkit as tk

_logger = logging.getLogger(__name__)

_WOLFRAM_ALPHA_APP_ID='V36VH4-56K3XYX347'

@asyncio.coroutine
def wolfram_query(query):
    headers = {"Content-Type" : "application/x-www-form-urlencoded"}
    encoded_query = urllib.parse.quote_plus(query)
    formats = 'image,plaintext'  # there is also 'html' but it's not useful for us
    url = 'http://api.wolframalpha.com/v2/query?appid={}&input={}&format={}'.format(
        _WOLFRAM_ALPHA_APP_ID, encoded_query, formats)
    with (yield from tk.retry_request('GET', url, timeout=30, headers=headers)) as resp:
        status = resp.status
        if status == 200:
            raw = yield from resp.read()
            return WolframResult(raw)
        else:
            _logger.error("Wolfram fetch failed: status=%d", status)

def first_or_none(iter):
    if iter is None:
        return None
    try:
        return next(iter)
    except StopIteration:
        return None

def as_ul(iter):
    items = ("- {}".format(x) for x in iter)
    return "{}".format('\n'.join(items))

class WolframResult:

    def __init__(self, raw):
        self.raw = raw
        self.success = False
        self.result = None
        self.result_text = None
        self.result_img = None
        self.result_sources = []

        if raw:
            self.process(xmltodict.parse(raw))

    def process(self, parsed):
        import pprint
        logging.debug(pprint.pformat(parsed))

        query_result = parsed.get('queryresult')
        self.success = query_result.get('@success') == 'true'
        if not self.success:
            return

        pods = query_result.get('pod', [])
        self.result = first_or_none(x for x in pods if x.get('@primary') == 'true')
        if not self.result:
            self.result = first_or_none(x for x in pods if x.get('@id') != 'Input')

        # We can ask for html, but it's so complicated we can't show it in HipChat
        # self.result_html = tk.nested_get(self.result, 'markup') or 'No idea'
        subpods = self.result.get('subpod', [])
        if isinstance(subpods, dict):
            self.result_text = subpods.get('plaintext') or ''
            self.result_img = subpods.get('img') or ''
        else:
            self.result_text = as_ul(x.get('plaintext') for x in subpods)
            self.result_img = None

        sources = tk.nested_get(query_result, 'sources', 'source')
        if sources:
            if isinstance(sources, dict):
                self.result_sources = [sources.get('@url')]
            else:
                self.result_sources = [x.get('@url') for x in sources]

    def __str__(self):
        return '{}(success={}, text={}, img={}, sources={})'.format(
            self.__class__.__name__,
            self.success,
            self.result_text,
            self.result_img,
            self.result_sources
        )

if __name__ == '__main__':

    def test_fetching():
        import sys
        from pprint import pprint

        log_format = '[%(asctime)s] [%(process)d] [%(name)s] %(message)s'

        logging.basicConfig(level=logging.DEBUG, stream=sys.stdout, format=log_format)

        loop = asyncio.get_event_loop()
        # loop.set_debug(True)

        @asyncio.coroutine
        def run_test():
            # x = yield from wolfram_query('how much vitamin E is there in a million croissants') # ('what is the unladen weight of a sparrow?')
            # x = yield from wolfram_query('how many Star Trek episodes are there?')
            x = yield from wolfram_query('compare Flintstones vs the Simpsons')
            print(x)

        t = loop.create_task(run_test())

        try:
            loop.run_until_complete(t)
        except KeyboardInterrupt:
            pass
        loop.close()

    def _parsing_input():

        return {'queryresult': {'@success': 'true',
                 '@error': 'false',
                 '@numpods': '2',
                 '@datatypes': 'HistoricalEvent',
                 '@timedout': '',
                 '@timedoutpods': '',
                 '@timing': '1.171',
                 '@parsetiming': '0.417',
                 '@parsetimedout': 'false',
                 '@recalculate': '',
                 '@id': 'MSPa30412046hce1fff9hg8a00003464ch520875h8ce',
                 '@host': 'http://www5a.wolframalpha.com',
                 '@server': '50',
                 '@related': 'http://www5a.wolframalpha.com/api/v2/relatedQueries.jsp?id=MSPa30422046hce1fff9hg8a000055cd0cig2159e4db&s=50',
                 '@version': '2.6',
                 'pod': [{'@title': 'Input interpretation',
                          '@scanner': 'Identity',
                          '@id': 'Input',
                          '@position': '100',
                          '@error': 'false',
                          '@numsubpods': '1',
                          'subpod': {'@title': '',
                                     'plaintext': 'August 1969 | events '
                                                  'and anniversaries',
                                     'img': {'@src': 'http://www5a.wolframalpha.com/Calculate/MSP/MSP30432046hce1fff9hg8a0000436hbcdb21034hb4?MSPStoreType=image/gif&s=50',
                                             '@alt': 'August 1969 | '
                                                     'events and '
                                                     'anniversaries',
                                             '@title': 'August 1969 | '
                                                       'events and '
                                                       'anniversaries',
                                             '@width': '274',
                                             '@height': '23'}}},
                         {'@title': 'Notable events in August 1969',
                          '@scanner': 'Date',
                          '@id': 'NotableEventForDate',
                          '@position': '200',
                          '@error': 'false',
                          '@numsubpods': '5',
                          'subpod': [{'@title': '',
                                      'plaintext': 'August 9: Tate and '
                                                   'LaBianca murders',
                                      'img': {'@src': 'http://www5a.wolframalpha.com/Calculate/MSP/MSP30442046hce1fff9hg8a00003b1if7bd19efh19c?MSPStoreType=image/gif&s=50',
                                              '@alt': 'August 9: Tate '
                                                      'and LaBianca '
                                                      'murders',
                                              '@title': 'August 9: '
                                                        'Tate and '
                                                        'LaBianca '
                                                        'murders',
                                              '@width': '250',
                                              '@height': '18'}},
                                     {'@title': '',
                                      'plaintext': 'August 15  to  '
                                                   'August 18, 1969: '
                                                   'Woodstock Music & '
                                                   'Art Fair',
                                      'img': {'@src': 'http://www5a.wolframalpha.com/Calculate/MSP/MSP30452046hce1fff9hg8a000044cdb33hh6578e70?MSPStoreType=image/gif&s=50',
                                              '@alt': 'August 15  to  '
                                                      'August 18, '
                                                      '1969: Woodstock '
                                                      'Music & Art Fair',
                                              '@title': 'August 15  '
                                                        'to  August '
                                                        '18, 1969: '
                                                        'Woodstock '
                                                        'Music & Art '
                                                        'Fair',
                                              '@width': '397',
                                              '@height': '18'}},
                                     {'@title': '',
                                      'plaintext': 'August 17: '
                                                   'Hurricane Camille '
                                                   'makes landfall in '
                                                   'Louisiana',
                                      'img': {'@src': 'http://www5a.wolframalpha.com/Calculate/MSP/MSP30462046hce1fff9hg8a00006b0hg813f6h7ee16?MSPStoreType=image/gif&s=50',
                                              '@alt': 'August 17: '
                                                      'Hurricane '
                                                      'Camille makes '
                                                      'landfall in '
                                                      'Louisiana',
                                              '@title': 'August 17: '
                                                        'Hurricane '
                                                        'Camille makes '
                                                        'landfall in '
                                                        'Louisiana',
                                              '@width': '382',
                                              '@height': '18'}},
                                     {'@title': '',
                                      'plaintext': 'August 18: birth of '
                                                   'Edward Norton '
                                                   '(1969- )',
                                      'img': {'@src': 'http://www5a.wolframalpha.com/Calculate/MSP/MSP30472046hce1fff9hg8a00000i96908e5d6ffhc4?MSPStoreType=image/gif&s=50',
                                              '@alt': 'August 18: '
                                                      'birth of Edward '
                                                      'Norton (1969- ) ',
                                              '@title': 'August 18: '
                                                        'birth of '
                                                        'Edward Norton '
                                                        '(1969- ) ',
                                              '@width': '296',
                                              '@height': '18'}},
                                     {'@title': '',
                                      'plaintext': 'August 28: birth of '
                                                   'Jack Black (1969- )',
                                      'img': {'@src': 'http://www5a.wolframalpha.com/Calculate/MSP/MSP30482046hce1fff9hg8a00003877a2db437ci0e3?MSPStoreType=image/gif&s=50',
                                              '@alt': 'August 28: '
                                                      'birth of Jack '
                                                      'Black (1969- ) ',
                                              '@title': 'August 28: '
                                                        'birth of Jack '
                                                        'Black (1969- '
                                                        ') ',
                                              '@width': '263',
                                              '@height': '18'}}],
                          'states': {'@count': '1',
                                     'state': {'@name': 'More',
                                               '@input': 'NotableEventForDate__More'}}}],
                 'sources': {'@count': '1',
                             'source': {'@url': 'http://www.wolframalpha.com/sources/PeopleDataSourceInformationNotes.html',
                                        '@text': 'People data'}}}}

    def _parsing_input2():
        return {'queryresult': {'@success': 'true',
                 '@error': 'false',
                 '@numpods': '5',
                 '@datatypes': 'ExpandedFood',
                 '@timedout': '',
                 '@timedoutpods': 'Average nutrition facts,Average daily '
                                  'value ranking,Average highest nutrients '
                                  'compared to other '
                                  'foods,Vitamins,Physical properties',
                 '@timing': '10.208',
                 '@parsetiming': '0.854',
                 '@parsetimedout': 'false',
                 '@recalculate': '',
                 '@id': 'MSPa26820f43ff335a862ag0000280fbh4cd7fbf17c',
                 '@host': 'http://www5a.wolframalpha.com',
                 '@server': '59',
                 '@related': 'http://www5a.wolframalpha.com/api/v2/relatedQueries.jsp?id=MSPa26920f43ff335a862ag000053c832eg9389gfe3&s=59',
                 '@version': '2.6',
                 'pod': [{'@title': 'Input interpretation',
                          '@scanner': 'Identity',
                          '@id': 'Input',
                          '@position': '100',
                          '@error': 'false',
                          '@numsubpods': '1',
                          'markup': '<div id="pod_0100" class="pod "><hr '
                                    'class="top" /><h2>\n'
                                    '      Input interpretation<span '
                                    'class="colon">:</span></h2><div '
                                    'id="subpod_0100_1" class="sub "><div '
                                    'class="output pnt" '
                                    'id="scannerresult_0100_1"><img '
                                    'height="32."width="392." '
                                    'src="http://www5a.wolframalpha.com/Calculate/MSP/MSP27320f43ff335a862ag00003addah4c03ib87ed?MSPStoreType=image/gif&s=59&amp;w=392.&amp;h=32." '
                                    'id="i_0100_1" alt="orange | amount | '
                                    '100 t  (metric tons) | vitamin C" '
                                    'title="orange | amount | 100 t  '
                                    '(metric tons) | vitamin C"  '
                                    'data-attribution="" /></div><div '
                                    'class="annotpod">\n'
                                    '\t</div></div>\n'
                                    '<hr class="bot" /></div>',
                          'subpod': {'@title': '',
                                     'plaintext': 'orange | amount | 100 '
                                                  't  (metric tons) | '
                                                  'vitamin C',
                                     'img': {'@src': 'http://www5a.wolframalpha.com/Calculate/MSP/MSP27420f43ff335a862ag00001h1dfh96c429i2i3?MSPStoreType=image/gif&s=59',
                                             '@alt': 'orange | amount | '
                                                     '100 t  (metric '
                                                     'tons) | vitamin C',
                                             '@title': 'orange | amount '
                                                       '| 100 t  '
                                                       '(metric tons) | '
                                                       'vitamin C',
                                             '@width': '392',
                                             '@height': '32'}}},
                         {'@title': 'Average result',
                          '@scanner': 'Data',
                          '@id': 'Result',
                          '@position': '200',
                          '@error': 'false',
                          '@numsubpods': '1',
                          '@primary': 'true',
                          'markup': '<div id="pod_0200" class="pod "><hr '
                                    'class="top" /><h2>\n'
                                    '      Average result<span '
                                    'class="colon">:</span></h2><ul '
                                    'class="h"><li class="first btn"><a '
                                    'href="pod.jsp?id=MSP27620f43ff335a862ag000049ga53fe44686agh&amp;s=59&amp;button=1false" '
                                    'id="substitute_0200_0">Show '
                                    'details</a></li></ul><div '
                                    'id="subpod_0200_1" class="sub "><div '
                                    'class="output pnt" '
                                    'id="scannerresult_0200_1"><img '
                                    'height="18."width="110." '
                                    'src="http://www5a.wolframalpha.com/Calculate/MSP/MSP27920f43ff335a862ag00004062e7h7317f3efb?MSPStoreType=image/gif&s=59&amp;w=110.&amp;h=18." '
                                    'id="i_0200_1" alt="55 kg  '
                                    '(kilograms)" title="55 kg  '
                                    '(kilograms)"  data-attribution="" '
                                    '/></div><div class="annotpod">\n'
                                    '\t</div></div>\n'
                                    '<hr class="bot" /></div>',
                          'subpod': {'@title': '',
                                     'plaintext': '55 kg  (kilograms)',
                                     'img': {'@src': 'http://www5a.wolframalpha.com/Calculate/MSP/MSP28020f43ff335a862ag00002c383h05eh4h0dg9?MSPStoreType=image/gif&s=59',
                                             '@alt': '55 kg  (kilograms)',
                                             '@title': '55 kg  '
                                                       '(kilograms)',
                                             '@width': '110',
                                             '@height': '18'}},
                          'states': {'@count': '1',
                                     'state': {'@name': 'Show details',
                                               '@input': 'Result__Show '
                                                         'details'}}},
                         {'@title': 'Unit conversions',
                          '@scanner': 'Unit',
                          '@id': 'UnitConversion',
                          '@position': '300',
                          '@error': 'false',
                          '@numsubpods': '2',
                          'markup': '<div id="pod_0300" class="pod "><hr '
                                    'class="top" /><h2>\n'
                                    '      Unit conversions<span '
                                    'class="colon">:</span></h2><div '
                                    'id="subpod_0300_1" class="sub "><div '
                                    'class="output pnt" '
                                    'id="scannerresult_0300_1"><img '
                                    'height="18."width="101." '
                                    'src="http://www5a.wolframalpha.com/Calculate/MSP/MSP28420f43ff335a862ag0000274hgi24i675905e?MSPStoreType=image/gif&s=59&amp;w=101.&amp;h=18." '
                                    'id="i_0300_1" alt="120 lb  (pounds)" '
                                    'title="120 lb  (pounds)"  '
                                    'data-attribution="" /></div><div '
                                    'class="annotpod">\n'
                                    '\t </div></div>\n'
                                    '<hr class="div" style="clear: both" '
                                    '/><div id="subpod_0300_2" class="sub '
                                    '"><div class="output pnt" '
                                    'id="scannerresult_0300_2"><img '
                                    'height="18."width="84." '
                                    'src="http://www5a.wolframalpha.com/Calculate/MSP/MSP28720f43ff335a862ag00005f5h68d4beh4iha4?MSPStoreType=image/gif&s=59&amp;w=84.&amp;h=18." '
                                    'id="i_0300_2" alt="55000 grams" '
                                    'title="55000 grams"  '
                                    'data-attribution="" /></div><div '
                                    'class="annotpod">\n'
                                    '\t</div></div>\n'
                                    '<hr class="bot" /></div>',
                          'subpod': [{'@title': '',
                                      'plaintext': '120 lb  (pounds)',
                                      'img': {'@src': 'http://www5a.wolframalpha.com/Calculate/MSP/MSP28820f43ff335a862ag0000577beef618f1a76a?MSPStoreType=image/gif&s=59',
                                              '@alt': '120 lb  (pounds)',
                                              '@title': '120 lb  '
                                                        '(pounds)',
                                              '@width': '101',
                                              '@height': '18'}},
                                     {'@title': '',
                                      'plaintext': '55000 grams',
                                      'img': {'@src': 'http://www5a.wolframalpha.com/Calculate/MSP/MSP28920f43ff335a862ag00003aec9d7h8344hg1i?MSPStoreType=image/gif&s=59',
                                              '@alt': '55000 grams',
                                              '@title': '55000 grams',
                                              '@width': '84',
                                              '@height': '18'}}]},
                         {'@title': 'Comparison to reference daily intake',
                          '@scanner': 'Data',
                          '@id': 'RDVPod:VitaminC:ExpandedFoodData',
                          '@position': '400',
                          '@error': 'false',
                          '@numsubpods': '1',
                          'markup': '<div id="pod_0400" class="pod "><hr '
                                    'class="top" /><h2>\n'
                                    '      Comparison to reference daily '
                                    'intake<span '
                                    'class="colon">:</span></h2><div '
                                    'id="subpod_0400_1" class="sub "><div '
                                    'class="output pnt" '
                                    'id="scannerresult_0400_1"><img '
                                    'height="153."width="500." '
                                    'src="http://www5a.wolframalpha.com/Calculate/MSP/MSP29320f43ff335a862ag000038e4d8ifc87d7i3g?MSPStoreType=image/gif&s=59&amp;w=500.&amp;h=153." '
                                    'id="i_0400_1" alt=" | daily amount | '
                                    '% daily value\\nRDA | 60 mg\\/day  '
                                    '(milligrams per day) | '
                                    '9.2&times;10^7%\\nDRI (male) | 90 '
                                    'mg\\/day  (milligrams per day) | '
                                    '6.2&times;10^7%\\nDRI (female) | 75 '
                                    'mg\\/day  (milligrams per day) | '
                                    '7.4&times;10^7%\\n(RDA: recommended '
                                    'daily allowance;  DRI: dietary '
                                    'reference intake)" title=" | daily '
                                    'amount | % daily value..."  '
                                    'data-attribution="" /></div><div '
                                    'class="annotpod">\n'
                                    '\t</div></div>\n'
                                    '<hr class="bot" /></div>',
                          'subpod': {'@title': '',
                                     'plaintext': '| daily amount | % '
                                                  'daily value\n'
                                                  'RDA | 60 mg/day  '
                                                  '(milligrams per day) '
                                                  '| 9.2×10^7%\n'
                                                  'DRI (male) | 90 '
                                                  'mg/day  (milligrams '
                                                  'per day) | 6.2×10^7%\n'
                                                  'DRI (female) | 75 '
                                                  'mg/day  (milligrams '
                                                  'per day) | 7.4×10^7%\n'
                                                  '(RDA: recommended '
                                                  'daily allowance;  '
                                                  'DRI: dietary '
                                                  'reference intake)',
                                     'img': {'@src': 'http://www5a.wolframalpha.com/Calculate/MSP/MSP29420f43ff335a862ag000033i5ae3cegdi294h?MSPStoreType=image/gif&s=59',
                                             '@alt': ' | daily amount | '
                                                     '% daily value RDA '
                                                     '| 60 mg/day  '
                                                     '(milligrams per '
                                                     'day) | 9.2×10^7% '
                                                     'DRI (male) | 90 '
                                                     'mg/day  '
                                                     '(milligrams per '
                                                     'day) | 6.2×10^7% '
                                                     'DRI (female) | 75 '
                                                     'mg/day  '
                                                     '(milligrams per '
                                                     'day) | 7.4×10^7% '
                                                     '(RDA: recommended '
                                                     'daily allowance;  '
                                                     'DRI: dietary '
                                                     'reference intake)',
                                             '@title': ' | daily amount '
                                                       '| % daily value '
                                                       'RDA | 60 '
                                                       'mg/day  '
                                                       '(milligrams per '
                                                       'day) | '
                                                       '9.2×10^7% DRI '
                                                       '(male) | 90 '
                                                       'mg/day  '
                                                       '(milligrams per '
                                                       'day) | '
                                                       '6.2×10^7% DRI '
                                                       '(female) | 75 '
                                                       'mg/day  '
                                                       '(milligrams per '
                                                       'day) | '
                                                       '7.4×10^7% (RDA: '
                                                       'recommended '
                                                       'daily '
                                                       'allowance;  '
                                                       'DRI: dietary '
                                                       'reference '
                                                       'intake)',
                                             '@width': '500',
                                             '@height': '153'}}},
                         {'@title': 'Ranking among common foods',
                          '@scanner': 'Data',
                          '@id': 'NutrientStatisticsPod:VitaminC:ExpandedFoodData',
                          '@position': '500',
                          '@error': 'false',
                          '@numsubpods': '1',
                          'markup': '<div id="pod_0500" class="pod "><hr '
                                    'class="top" /><h2>\n'
                                    '      Ranking among common '
                                    'foods<span '
                                    'class="colon">:</span></h2><ul '
                                    'class="h"><li class="first btn"><a '
                                    'href="pod.jsp?id=MSP29620f43ff335a862ag00003bb7af5c97145h81&amp;s=59&amp;button=1false" '
                                    'id="substitute_0500_0">Use linear '
                                    'scale</a></li></ul><div '
                                    'id="subpod_0500_1" class="sub "><div '
                                    'class="output pnt" '
                                    'id="scannerresult_0500_1"><img '
                                    'height="241."width="463." '
                                    'src="http://www5a.wolframalpha.com/Calculate/MSP/MSP29920f43ff335a862ag00000haa52id9ibdefd2?MSPStoreType=image/gif&s=59&amp;w=463.&amp;h=241." '
                                    'id="i_0500_1" alt="fraction with '
                                    'higher value | 0%\\nfraction with '
                                    'lower value | 100%\\nfraction with '
                                    'zero value | 45%\\ndistribution | '
                                    '\\n(distribution is based on '
                                    'nutrient values in standard servings '
                                    'of 7500 common foods)" '
                                    'title="fraction with higher value | '
                                    '0%..."  data-attribution="" '
                                    '/></div><div class="annotpod">\n'
                                    '\t</div></div>\n'
                                    '<hr class="bot" /></div>',
                          'subpod': {'@title': '',
                                     'plaintext': 'fraction with higher '
                                                  'value | 0%\n'
                                                  'fraction with lower '
                                                  'value | 100%\n'
                                                  'fraction with zero '
                                                  'value | 45%\n'
                                                  'distribution | \n'
                                                  '(distribution is '
                                                  'based on nutrient '
                                                  'values in standard '
                                                  'servings of 7500 '
                                                  'common foods)',
                                     'img': {'@src': 'http://www5a.wolframalpha.com/Calculate/MSP/MSP30020f43ff335a862ag00002h4180b4dbffbh54?MSPStoreType=image/gif&s=59',
                                             '@alt': 'fraction with '
                                                     'higher value | 0% '
                                                     'fraction with '
                                                     'lower value | '
                                                     '100% fraction '
                                                     'with zero value | '
                                                     '45% distribution '
                                                     '|  (distribution '
                                                     'is based on '
                                                     'nutrient values '
                                                     'in standard '
                                                     'servings of 7500 '
                                                     'common foods)',
                                             '@title': 'fraction with '
                                                       'higher value | '
                                                       '0% fraction '
                                                       'with lower '
                                                       'value | 100% '
                                                       'fraction with '
                                                       'zero value | '
                                                       '45% '
                                                       'distribution |  '
                                                       '(distribution '
                                                       'is based on '
                                                       'nutrient values '
                                                       'in standard '
                                                       'servings of '
                                                       '7500 common '
                                                       'foods)',
                                             '@width': '463',
                                             '@height': '241'}},
                          'states': {'@count': '1',
                                     'state': {'@name': 'Use linear '
                                                        'scale',
                                               '@input': 'NutrientStatisticsPod:VitaminC:ExpandedFoodData__Use '
                                                         'linear scale'}}}],
                 'assumptions': {'@count': '2',
                                 'assumption': [{'@type': 'Unit',
                                                 '@word': 'tonnes',
                                                 '@template': 'Assuming '
                                                              '${desc1} '
                                                              'for '
                                                              '"${word}". '
                                                              'Use '
                                                              '${desc2} '
                                                              'instead',
                                                 '@count': '4',
                                                 'value': [{'@name': 'MetricTons',
                                                            '@desc': 'metric '
                                                                     'tons',
                                                            '@input': 'UnitClash_*tonnes.*MetricTons--'},
                                                           {'@name': 'LongTons',
                                                            '@desc': 'long '
                                                                     'tons',
                                                            '@input': 'UnitClash_*tonnes.*LongTons--'},
                                                           {'@name': 'AssayLongTons',
                                                            '@desc': 'long '
                                                                     'assay '
                                                                     'tons',
                                                            '@input': 'UnitClash_*tonnes.*AssayLongTons--'},
                                                           {'@name': 'ShortTons',
                                                            '@desc': 'short '
                                                                     'tons',
                                                            '@input': 'UnitClash_*tonnes.*ShortTons--'}]},
                                                {'@type': 'Attribute',
                                                 '@word': 'Orange',
                                                 '@template': 'Assuming '
                                                              '${desc1}. '
                                                              'Use '
                                                              '${desc2} '
                                                              'instead',
                                                 '@count': '6',
                                                 'value': [{'@name': 'Orange',
                                                            '@desc': 'any '
                                                                     'type '
                                                                     'of '
                                                                     'orange',
                                                            '@input': '*EAC.ExpandedFood.Orange-_**a.Orange--'},
                                                           {'@name': '{Food:Type '
                                                                     '-> '
                                                                     'Food:Generic}',
                                                            '@desc': 'orange, '
                                                                     'generic',
                                                            '@input': '*EAC.ExpandedFood.Orange-_**Orange.*Food%3AType_Food%3AGeneric---'},
                                                           {'@name': '{Food:Variety '
                                                                     '-> '
                                                                     'Food:Navels}',
                                                            '@desc': 'orange, '
                                                                     'navels',
                                                            '@input': '*EAC.ExpandedFood.Orange-_**Orange.*Food%3AVariety_Food%3ANavels---'},
                                                           {'@name': '{Food:Variety '
                                                                     '-> '
                                                                     'Food:Florida}',
                                                            '@desc': 'orange, '
                                                                     'florida',
                                                            '@input': '*EAC.ExpandedFood.Orange-_**Orange.*Food%3AVariety_Food%3AFlorida---'},
                                                           {'@name': '{Food:PeelingType '
                                                                     '-> '
                                                                     'Food:WithPeel}',
                                                            '@desc': 'orange, '
                                                                     'with '
                                                                     'peel',
                                                            '@input': '*EAC.ExpandedFood.Orange-_**Orange.*Food%3APeelingType_Food%3AWithPeel---'},
                                                           {'@name': '{Food:Variety '
                                                                     '-> '
                                                                     'Food:California, '
                                                                     'Food:Variety '
                                                                     '-> '
                                                                     'Food:Valencias}',
                                                            '@desc': 'orange, '
                                                                     'California, '
                                                                     'valencias',
                                                            '@input': '*EAC.ExpandedFood.Orange-_**Orange.*Food%3AVariety_Food%3ACalifornia.Food%3AVariety_Food%3AValencias---'}]}]},
                 'sources': {'@count': '1',
                             'source': {'@url': 'http://www.wolframalpha.com/sources/ExpandedFoodDataSourceInformationNotes.html',
                                        '@text': 'Expanded food data'}},
                }}

    def test_parsing():
        d = _parsing_input2()
        r = WolframResult('')
        r.process(d)
        print(r)

if __name__ == '__main__':
    test_fetching()
#    test_parsing()
