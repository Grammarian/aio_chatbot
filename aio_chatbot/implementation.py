import asyncio
import logging

from datetime import datetime

_logger = logging.getLogger(__name__)


class BrilliantImplementation:
    """
    This class does the actual work of calculating something brilliant
    """

    def __init__(self, installation):
        self.installation = installation

    @asyncio.coroutine
    def get_glance_data(self):
        """
        Return a dictionary which holds the information to be put into a glance
        """
        data = {
            'label': '<i>SIMPLE<i>',  # the label can have simple HTML
            'lozenge_text': '!',
            'lozenge_type': 'error'
        }

        return data

    @asyncio.coroutine
    def get_sidebar_data(self):
        """
        Return a dictionary of values used by sidebar.jinja2 to generate the html for the sidebar
        """
        data = {
            'now': str(datetime.now()),
        }

        return data
