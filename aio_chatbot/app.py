from aio_hipchat.hc_addon_app import HcAddonApp
from aio_hipchat.config_generator import ConfigGenerator

from aio_chatbot import app_api

app_config = {
    "DEBUG": True,
    "ADDON_KEY": "simple-brilliance-chatbot",
    "ADDON_NAME": "Chatting with bots in HipChat",
    "ADDON_DESCRIPTION": "Chatting with bots to win friends and influence people",
    "ADDON_VENDOR_NAME": "Atlassian",
    "ADDON_VENDOR_URL": "http://atlassian.com",
    "ADDON_FROM_NAME": "Phillip Piper",
    "ADDON_AVATAR": "/static/robot-1.png",
    "ADDON_AVATAR_2X": "/static/robot-1-2x.png",
    "ADDON_ALLOW_ROOM": "true",
    "ADDON_ALLOW_GLOBAL": "false",
    "ADDON_SCOPES": "send_notification view_group view_room",  # space-separated list of scopes required by the add on

    "APP_NAME": "SimplyBrilliantChatBot",
    "BASE_URL": "http://127.0.0.1:28865",
    "REDIS_URL": "redis://localhost:6379",
    "MONGO_URL": "mongodb://localhost:27017/test",
}

app = HcAddonApp(__file__, ConfigGenerator(app_config))
hc_api = app_api.SampleApi(app)
