A Simple HipChat Chatbot
========================

This is a HipChat chat bot, which can converse directly with a HipChat user.

```sh
$ cd /mnt/hipchat/aio_chatbot
$ virtualenv -p python3 venv
$ source venv/bin/activate
$ pip install -r dev-requirements.txt --index-url https://pypi.python.org/simple --trusted-host pypi.python.org 
$ BASE_URL=http://192.168.33.1:28865; export BASE_URL
```

To run it:

```sh
$ gunicorn -k aiohttp.worker.GunicornWebWorker aio_chatbot.app:app -b 0.0.0.0:28865 --reload
```
